<?php

namespace SJRoyd\MF\VATPayers;

use SJRoyd\HTTPService\RestRequest;

abstract class API extends RestRequest
{
    protected $ws_path = [
        'prod' => 'https://wl-api.mf.gov.pl/api',
        'test' => 'https://wl-test.mf.gov.pl/api'
    ];

    /**
     * API constructor.
     *
     * @param   bool  $test
     * @param   bool  $debug
     */
    public function __construct($test = false, $debug = false)
    {
        parent::__construct($test, $debug);
        $this->ws_path = $this->ws_path[$test ? 'test' : 'prod'];
    }

    /**
     * @param   string|null|\DateTime  $date
     *
     * @return string
     * @throws \Exception
     */
    protected function getDate($date)
    {
        if ( ! $date instanceof \DateTime) {
            $date = new \DateTime($date);
        }

        return $date->format('Y-m-d');
    }

}