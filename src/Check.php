<?php

namespace SJRoyd\MF\VATPayers;

use SJRoyd\MF\VATPayers\Check\Response;
use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Params\NIP;
use SJRoyd\MF\VATPayers\Params\REGON;

class Check extends API
{
    protected $ws_name = 'check';

    /**
     * Single entity check by NIP and bank account number
     *
     * @param   NIP                    $nip
     * @param   NRB                    $bankAccount
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityCheck
     * @throws Exception
     * @throws \Exception
     */
    public function nip(NIP $nip, NRB $bankAccount, $date = null)
    {
        /** @var Response\EntityCheckResponse|Exception $response */
        $response = $this->call(
            "nip/{$nip}/bank-account/{$bankAccount}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityCheckResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Single entity check by REGON and bank account number
     *
     * @param   REGON                  $regon
     * @param   NRB                    $bankAccount
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityCheck
     * @throws Exception
     * @throws \Exception
     */
    public function regon(REGON $regon, NRB $bankAccount, $date = null)
    {
        /** @var Response\EntityCheckResponse|Exception $response */
        $response = $this->call(
            "regon/{$regon}/bank-account/{$bankAccount}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityCheckResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }
}