<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

class EntityList
{
    use Request;

    /**
     * @var Entity[]
     */
    protected $subjects;

    /**
     * @return Entity[]
     */
    public function getSubjects()
    {
        return $this->subjects;
    }

    /**
     * @param   Entity[]  $subjects
     *
     * @return EntityList
     */
    public function setSubjects($subjects)
    {
        $this->subjects = $subjects;

        return $this;
    }

}