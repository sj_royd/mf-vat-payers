<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

class Entity
{
    const VAT_STATUS_ACTIVE = 'Czynny';
    const VAT_STATUS_EXEMPT = 'Zwolniony';
    const VAT_STATUS_UNREGISTERED = 'Niezarejestrowany';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $nip;

    /**
     * @var string|null
     */
    protected $statusVat;

    /**
     * @var string|null
     */
    protected $regon;

    /**
     * @var string|null
     */
    protected $pesel;

    /**
     * @var string|null
     */
    protected $krs;

    /**
     * @var string|null
     */
    protected $residenceAddress;

    /**
     * @var string|null
     */
    protected $workingAddress;

    /**
     * @var EntityPerson[]
     */
    protected $representatives = [];

    /**
     * @var EntityPerson[]
     */
    protected $authorizedClerks = [];

    /**
     * @var EntityPerson[]
     */
    protected $partners = [];

    /**
     * @var \DateTime|null
     */
    protected $registrationLegalDate;

    /**
     * @var \DateTime|null
     */
    protected $registrationDenialDate;

    /**
     * @var string|null
     */
    protected $registrationDenialBasis;

    /**
     * @var \DateTime|null
     */
    protected $restorationDate;

    /**
     * @var string|null
     */
    protected $restorationBasis;

    /**
     * @var \DateTime|null
     */
    protected $removalDate;

    /**
     * @var string|null
     */
    protected $removalBasis;

    /**
     * @var array
     */
    protected $accountNumbers = [];

    /**
     * @var bool|null
     */
    protected $hasVirtualAccounts;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param   string  $name
     *
     * @return Entity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param   string|null  $nip
     *
     * @return Entity
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusVat()
    {
        return $this->statusVat;
    }

    /**
     * @param   string|null  $statusVat
     *
     * @return Entity
     */
    public function setStatusVat($statusVat)
    {
        $this->statusVat = $statusVat;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegon()
    {
        return $this->regon;
    }

    /**
     * @param   string|null  $regon
     *
     * @return Entity
     */
    public function setRegon($regon)
    {
        $this->regon = $regon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * @param   string|null  $pesel
     *
     * @return Entity
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * @param   string|null  $krs
     *
     * @return Entity
     */
    public function setKrs($krs)
    {
        $this->krs = $krs;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResidenceAddress()
    {
        return $this->residenceAddress;
    }

    /**
     * @param   string|null  $residenceAddress
     *
     * @return Entity
     */
    public function setResidenceAddress($residenceAddress)
    {
        $this->residenceAddress = $residenceAddress;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkingAddress()
    {
        return $this->workingAddress;
    }

    /**
     * @param   string|null  $workingAddress
     *
     * @return Entity
     */
    public function setWorkingAddress($workingAddress)
    {
        $this->workingAddress = $workingAddress;

        return $this;
    }

    /**
     * @return EntityPerson[]
     */
    public function getRepresentatives()
    {
        return $this->representatives;
    }

    /**
     * @param   EntityPerson[]  $representatives
     *
     * @return Entity
     */
    public function setRepresentatives($representatives)
    {
        $this->representatives = $representatives;

        return $this;
    }

    /**
     * @return EntityPerson[]
     */
    public function getAuthorizedClerks()
    {
        return $this->authorizedClerks;
    }

    /**
     * @param   EntityPerson[]  $authorizedClerks
     *
     * @return Entity
     */
    public function setAuthorizedClerks($authorizedClerks)
    {
        $this->authorizedClerks = $authorizedClerks;

        return $this;
    }

    /**
     * @return EntityPerson[]
     */
    public function getPartners()
    {
        return $this->partners;
    }

    /**
     * @param   EntityPerson[]  $partners
     *
     * @return Entity
     */
    public function setPartners($partners)
    {
        $this->partners = $partners;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRegistrationLegalDate()
    {
        return $this->registrationLegalDate;
    }

    /**
     * @param   \DateTime|null  $registrationLegalDate
     *
     * @return Entity
     */
    public function setRegistrationLegalDate($registrationLegalDate)
    {
        $this->registrationLegalDate = $registrationLegalDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRegistrationDenialDate()
    {
        return $this->registrationDenialDate;
    }

    /**
     * @param   \DateTime|null  $registrationDenialDate
     *
     * @return Entity
     */
    public function setRegistrationDenialDate($registrationDenialDate)
    {
        $this->registrationDenialDate = $registrationDenialDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegistrationDenialBasis()
    {
        return $this->registrationDenialBasis;
    }

    /**
     * @param   string|null  $registrationDenialBasis
     *
     * @return Entity
     */
    public function setRegistrationDenialBasis($registrationDenialBasis)
    {
        $this->registrationDenialBasis = $registrationDenialBasis;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRestorationDate()
    {
        return $this->restorationDate;
    }

    /**
     * @param   \DateTime|null  $restorationDate
     *
     * @return Entity
     */
    public function setRestorationDate($restorationDate)
    {
        $this->restorationDate = $restorationDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRestorationBasis()
    {
        return $this->restorationBasis;
    }

    /**
     * @param   string|null  $restorationBasis
     *
     * @return Entity
     */
    public function setRestorationBasis($restorationBasis)
    {
        $this->restorationBasis = $restorationBasis;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRemovalDate()
    {
        return $this->removalDate;
    }

    /**
     * @param   \DateTime|null  $removalDate
     *
     * @return Entity
     */
    public function setRemovalDate($removalDate)
    {
        $this->removalDate = $removalDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRemovalBasis()
    {
        return $this->removalBasis;
    }

    /**
     * @param   string|null  $removalBasis
     *
     * @return Entity
     */
    public function setRemovalBasis($removalBasis)
    {
        $this->removalBasis = $removalBasis;

        return $this;
    }

    /**
     * @return array
     */
    public function getAccountNumbers()
    {
        return $this->accountNumbers;
    }

    /**
     * @param   array  $accountNumbers
     *
     * @return Entity
     */
    public function setAccountNumbers($accountNumbers)
    {
        $this->accountNumbers = $accountNumbers;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHasVirtualAccounts()
    {
        return $this->hasVirtualAccounts;
    }

    /**
     * @param   bool|null  $hasVirtualAccounts
     *
     * @return Entity
     */
    public function setHasVirtualAccounts($hasVirtualAccounts)
    {
        $this->hasVirtualAccounts = $hasVirtualAccounts;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVatActive()
    {
        return $this->statusVat == self::VAT_STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isVatExempt()
    {
        return $this->statusVat == self::VAT_STATUS_EXEMPT;
    }

    /**
     * @return bool
     */
    public function isVatUnregistered()
    {
        return $this->statusVat == self::VAT_STATUS_UNREGISTERED;
    }

}