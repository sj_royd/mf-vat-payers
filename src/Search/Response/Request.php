<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

trait Request
{
    /**
     * @var string|null
     */
    protected $requestId;

    /**
     * @return string|null
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param   string|null  $requestId
     *
     * @return Request
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

}