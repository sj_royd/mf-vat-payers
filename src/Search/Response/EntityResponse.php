<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

class EntityResponse
{
    /**
     * @var EntityItem
     */
    protected $result;

    /**
     * @return EntityItem
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param   EntityItem  $result
     *
     * @return EntityResponse
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

}