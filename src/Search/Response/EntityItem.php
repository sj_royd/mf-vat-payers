<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

class EntityItem
{
    use Request;

    /**
     * @var Entity|null
     */
    protected $subject;

    /**
     * @return Entity|null
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param   Entity|null  $subject
     *
     * @return EntityItem
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

}