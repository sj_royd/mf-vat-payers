<?php

namespace SJRoyd\MF\VATPayers\Search\Response;

class EntityPerson
{
    /**
     * @var string|null
     */
    protected $companyName;

    /**
     * @var string|null
     */
    protected $firstName;

    /**
     * @var string|null
     */
    protected $lastName;

    /**
     * @var string|null
     */
    protected $pesel;

    /**
     * @var string|null
     */
    protected $nip;

    /**
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param   string|null  $companyName
     *
     * @return EntityPerson
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param   string|null  $firstName
     *
     * @return EntityPerson
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param   string|null  $lastName
     *
     * @return EntityPerson
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * @param   string|null  $pesel
     *
     * @return EntityPerson
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param   string|null  $nip
     *
     * @return EntityPerson
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }


}