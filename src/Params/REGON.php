<?php

namespace SJRoyd\MF\VATPayers\Params;

use Kiczort\PolishValidator\RegonValidator;

class REGON extends Param
{
    /**
     * @param $number
     *
     * @throws InputException
     */
    protected function validate($number)
    {
        $number = $this->clearNumber($number);
        $validator = new RegonValidator();
        if ( ! $validator->isValid($number)) {
            throw new InputException("REGON number {$number} is invalid");
        }
    }
}