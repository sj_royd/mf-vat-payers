<?php

namespace SJRoyd\MF\VATPayers\Params;

use IBAN as IBANBase;

class NRB extends Param
{
    /**
     * @param $number
     *
     * @throws InputException
     */
    protected function validate($number)
    {
        $validator = new IBANBase('PL' . $number);
        if ( ! $validator->Verify()) {
            throw new InputException("Bank account number {$number} is invalid");
        }
    }
}