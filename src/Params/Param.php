<?php


namespace SJRoyd\MF\VATPayers\Params;


abstract class Param
{
    protected $list = [];

    /**
     * REGON constructor.
     *
     * @param   string|array  $input
     *
     * @throws InputException
     */
    public function __construct($input)
    {
        if (is_string($input)) {
            $this->validate($input);
            $this->list[] = $input;
        } elseif (is_array($input)) {
            array_walk($input, function ($number) {
                $this->validate($number);
            });
            $this->list = $input;
        } else {
            throw new InputException('Invalid input type');
        }
    }

    protected abstract function validate($number);

    /**
     * @return string
     */
    public function getNumber()
    {
        return reset($this->list);
    }

    /**
     * @return array
     */
    public function getNumbers()
    {
        return implode(',', $this->list);
    }

    /**
     * @param $number
     *
     * @return string|string[]|null
     */
    protected function clearNumber($number)
    {
        return preg_replace('~[^\d]~', '', $number);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNumber();
    }
}