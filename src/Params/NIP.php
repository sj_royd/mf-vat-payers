<?php

namespace SJRoyd\MF\VATPayers\Params;

use Kiczort\PolishValidator\NipValidator;

class NIP extends Param
{
    /**
     * @param $number
     *
     * @throws InputException
     */
    protected function validate($number)
    {
        $number = $this->clearNumber($number);
        $validator = new NipValidator();
        if ( ! $validator->isValid($number)) {
            throw new InputException("NIP number {$number} is invalid");
        }
    }
}