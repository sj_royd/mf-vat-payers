<?php

namespace SJRoyd\MF\VATPayers;

class Exception extends \Exception
{
    /**
     * @param   mixed  $message
     *
     * @return Exception
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param   string  $code
     *
     * @return Exception
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
}