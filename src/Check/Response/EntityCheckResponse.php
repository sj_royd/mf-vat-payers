<?php

namespace SJRoyd\MF\VATPayers\Check\Response;

class EntityCheckResponse
{
    /**
     * @var EntityCheck
     */
    protected $result;

    /**
     * @return EntityCheck
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param   EntityCheck  $result
     *
     * @return EntityCheckResponse
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }


}