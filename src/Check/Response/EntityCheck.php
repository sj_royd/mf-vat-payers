<?php

namespace SJRoyd\MF\VATPayers\Check\Response;

use SJRoyd\MF\VATPayers\Search\Response\Request;

class EntityCheck
{
    use Request;

    /**
     * @var string
     */
    protected $accountAssigned;

    /**
     * @return bool
     */
    public function getAccountAssigned()
    {
        return strtr($this->accountAssigned, [
            'TAK' => true,
            'NIE' => false
        ]);
    }

    /**
     * @param   string  $accountAssigned
     *
     * @return EntityCheck
     */
    public function setAccountAssigned($accountAssigned)
    {
        $this->accountAssigned = $accountAssigned;

        return $this;
    }


}