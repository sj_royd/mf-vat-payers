<?php

namespace SJRoyd\MF\VATPayers;

set_time_limit(0);

use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Params\NIP;

class CheckInFile
{
    /**
     * Data file location
     * @var string
     */
    protected static $fileDataLocation = '../bin/flat.data';

    /**
     * Hashes key in data array
     * @var string
     */
    private static $hashes = 'skróty';

    /**
     * Masks key in data array
     * @var string
     */
    private static $masks = 'maski';

    /**
     * Data array
     * @var array
     */
    private static $data;

    /**
     * @param   NIP  $nip
     * @param   NRB  $bankAccount
     *
     * @return bool
     */
    public static function check(NIP $nip, NRB $bankAccount)
    {
        self::$data = unserialize(
            file_get_contents(__DIR__ . '/' . self::$fileDataLocation)
        );

        $hash = self::prepareHash(
            $nip->getNumber(),
            $bankAccount->getNumber()
        );

        return self::checkDirectly($hash)
            ?: self::getByMask($nip, $bankAccount);
    }

    /**
     * @param $hash
     *
     * @return bool
     */
    private static function checkDirectly($hash)
    {
        return in_array($hash, self::$data[self::$hashes]);
    }

    /**
     * @param   NIP  $nip
     * @param   NRB  $bankAccount
     *
     * @return bool
     */
    private static function getByMask(NIP $nip, NRB $bankAccount)
    {
        $mask = self::findMask($bankAccount);
        if ( ! $mask) {
            return false;
        }

        $aMask = str_split($mask);
        $aNum = str_split($bankAccount->getNumber());
        foreach($aMask as $i => $p){
            if($p == 'Y'){
                $aMask[$i] = $aNum[$i];
            }
        }

        $hash = self::prepareHash(
            $nip->getNumber(),
            implode($aMask)
        );

        return self::checkDirectly($hash);
    }

    /**
     * @param   NRB  $bankAccount
     *
     * @return bool
     */
    private static function findMask(NRB $bankAccount)
    {
        $identifier = substr($bankAccount->getNumber(), 2, 8);

        foreach (self::$data[self::$masks] as $mask) {
            if (substr($mask, 2, 8) == $identifier) {
                return $mask;
            }
        }

        return false;
    }

    /**
     * @param   string  $nip
     * @param   string  $bankAccountNumber
     *
     * @return string
     */
    private static function prepareHash($nip, $bankAccountNumber)
    {
        $date = '20191018';

//        $date = date('Ymd');
        return hash('sha512', implode('', [
            $date,
            $nip,
            $bankAccountNumber
        ]));
    }

}