<?php

namespace SJRoyd\MF\VATPayers;

use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Params\NIP;
use SJRoyd\MF\VATPayers\Params\REGON;
use SJRoyd\MF\VATPayers\Search\Response;

class Search extends API
{
    protected $ws_name = 'search';

    /**
     * Searching for entities by bank account number
     *
     * @param   NRB                    $number  Bank account number
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityList
     * @throws Exception
     * @throws \Exception
     */
    public function bankAccount(NRB $number, $date = null)
    {
        /** @var Response\EntityListResponse|Exception $response */
        $response = $this->call(
            "bank-account/{$number}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityListResponse::class,
                400 => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Searching for entities by bank account numbers
     *
     * @param   NRB                    $numbers  List of up to 30 bank account numbers
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityList
     * @throws Exception
     * @throws \Exception
     */
    public function bankAccounts($numbers, $date = null)
    {
        /** @var Response\EntityListResponse|Exception $response */
        $response = $this->call(
            "bank-account/{$numbers->getNumbers()}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityListResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Searching for a single entity by tax identification number
     *
     * @param   NIP                    $number  NIP
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityItem
     * @throws Exception
     * @throws \Exception
     */
    public function nip(NIP $number, $date = null)
    {
        /** @var Response\EntityResponse|Exception $response */
        $response = $this->call(
            "nip/{$number}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Searching for entities by tax identification numbers
     *
     * @param   NIP                    $numbers  List of up to 30 NIPs
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityList
     * @throws Exception
     * @throws \Exception
     */
    public function nips(NIP $numbers, $date = null)
    {
        /** @var Response\EntityListResponse|Exception $response */
        $response = $this->call(
            "nips/{$numbers->getNumbers()}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityListResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Searching for a single entity by REGON number
     *
     * @param   REGON                  $number  REGON
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityItem
     * @throws Exception
     * @throws \Exception
     */
    public function regon(REGON $number, $date = null)
    {
        /** @var Response\EntityResponse|Exception $response */
        $response = $this->call(
            "regon/{$number}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }

    /**
     * Searching for entities by REGON numbers
     *
     * @param   REGON                  $numbers  REGONs
     * @param   null|string|\DateTime  $date
     *
     * @return Response\EntityList
     * @throws Exception
     * @throws \Exception
     */
    public function regons(REGON $numbers, $date = null)
    {
        /** @var Response\EntityListResponse|Exception $response */
        $response = $this->call(
            "regons/{$numbers->getNumbers()}",
            [
                'query' => [
                    'date' => $this->getDate($date)
                ]
            ],
            [
                200 => Response\EntityListResponse::class,
                '4..' => Exception::class
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response->getResult();
        } else {
            throw $response;
        }
    }
}