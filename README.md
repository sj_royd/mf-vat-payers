# VAT Payers

List of entities registered as VAT payers, unregistered, as well as deleted and restored to the VAT register.

The next step will be handling a flat file in which information about NIP-bank account pairs is to be provided. The Ministry has not yet provided the location of such a file.

More on https://www.gov.pl/web/kas/wykaz-podatnikow-vat

## Usage

### Search methods

**The ministry imposed a limit of 10 questions per day on single IP.**

#### nip method

Searching for a single entity by tax identification number

```php
<?php

use SJRoyd\MF\VATPayers\Params\NIP;
use SJRoyd\MF\VATPayers\Search;
    
try {
    $s = new Search();
    $r = $s->nip(new NIP('0000000000'));
    // $r is a EntityItem instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```

#### nips method

Searching for entities by tax identification numbers

```php
<?php

use SJRoyd\MF\VATPayers\Params\NIP;
use SJRoyd\MF\VATPayers\Search;

try {
    $s = new Search();
    $r = $s->nips(new NIP(['0000000000', '1111111111'])); // max 30 numbers
    // $r is a EntityList instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```

#### regon method

Searching for a single entity by REGON number

```php
<?php

use SJRoyd\MF\VATPayers\Params\REGON;
use SJRoyd\MF\VATPayers\Search;

try {
    $s = new Search();
    $r = $s->regon(new REGON('000000000')); // 9 or 14 digits REGON
    // $r is a EntityItem instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```
#### regons method

Searching for entities by REGON numbers

```php
<?php

use SJRoyd\MF\VATPayers\Params\REGON;
use SJRoyd\MF\VATPayers\Search;

try {
    $s = new Search();
    $r = $s->regons(new REGON(['000000000', '11111111111111'])); // 9 or 14 digits REGONs, max 30 numbers
    // $r is a EntityList instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```

#### bankAccount method

Searching for entities by bank account number

```php
<?php

use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Search;

try {
    $s = new Search();
    $r = $s->bankAccount(new NRB('00 0000 0000 0000 0000 0000 0000')); // 26 digits polish bank account number
    // $r is a EntityList instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```

#### bankAccounts method

Searching for entities by bank account numbers

```php
<?php

use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Search;

try {
    $s = new Search();
    $r = $s->bankAccounts(new NRB(['00 0000 0000 0000 0000 0000 0000', '12345678901234567890123456'])); // max 30 numbers
    // $r is a EntityList instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```


### Check methods

**The ministry imposed a limit of 1 question per day on IP.**

#### nip method

Single entity check by NIP and bank account number

```php
<?php

use SJRoyd\MF\VATPayers\Params\NIP;
use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Check;

try {
    $c = new Check();
    $r = $c->nip(new NIP('0000000000'), new NRB('00 0000 0000 0000 0000 0000 0000'));
    // $r is a EntityCheck instance  
} catch (Exception $e){
    // $e is a Exception instance
}
```

#### regon method

Single entity check by REGON and bank account number

```php
<?php

use SJRoyd\MF\VATPayers\Params\REGON;
use SJRoyd\MF\VATPayers\Params\NRB;
use SJRoyd\MF\VATPayers\Check;
use SJRoyd\MF\VATPayers\Exception;

try {
    $c = new Check();
    $r = $c->regon(new REGON('000000000'), new NRB('00 0000 0000 0000 0000 0000 0000'));
    // $r is a EntityCheck instance 
} catch (Exception $e){
    // $e is a Exception instance
}
```

## Responses

### EntityList

Contains methods:

- `getSubjects(): array[Entity]` - A list of Entity instances
- `getRequestId(): string` - Eequest id, ex. `9mll9-85feng0`

### EntityItem

Contains methods:

- `getSubject(): Entity` - An Entity instance
- `getRequestId(): string` - Eequest id, ex. `9mll9-85feng0`

### EntityCheck

Contains methods:

- `getAccountAssigned(): boolean` - Is the account assigned to the active entity
- `getRequestId(): string` - Eequest id, ex. `9mll9-85feng0`

## Entities

### Entity

Contains methods:

- `getName(): string` - Company (name) or name and surname
- `getNip(): string` - (optional) NIP identification number
- `getStatusVat(): string` - (optional) VAT payer status; Enum: `Czynny`, `Zwolniony`, `Niezarejestrowany`
- `getRegon(): string` - (optional) REGON identification number
- `getPesel(): string -` (optional) PESEL identification number
- `getKrs(): string` - (optional) KRS number if issued
- `getResidenceAddress(): string` - (optional) HQ adress
- `getWorkingAddress(): string` - (optional) Permanent establishment address or residence address in the absence of a permanent address
- `getRepresentatives(): array[EntityPerson]` - (optional) Names and surnames of the members of the body authorized to represent the entity and their NIP and/or PESEL numbers
- `getAuthorizedClerks(): array[EntityPerson]` - (optional) First and last names of proxies and their NIP and/or PESEL numbers
- `getPartners(): array[EntityPerson]` - (optional) First and last names or company (name) of the partner and his NIP and/or PESEL numbers
- `getRegistrationLegalDate(): DateTime` - (optional) Date of registration as a VAT payer
- `getRegistrationDenialDate(): DateTime` - (optional) Date of refusal to register as a VAT payer
- `getRegistrationDenialBasis(): string` - (optional) Legal basis for refusal of registration
- `getRestorationDate(): DateTime` - (optional) Date of restoration as a VAT payer
- `getRestorationBasis(): string -` (optional) Legal basis for the restoration as a VAT payer
- `getRemovalDate(): DateTime` - (optional) Date of deletion of refusal to register as a VAT payer
- `getRemovalBasis(): string` - (optional) Legal basis for the restoration as a VAT payer
- `getAccountNumbers(): array[string]` - (optional) A list of bank accounts numbers
- `getHasVirtualAccounts(): boolean` - (optional) The entity has virtual account masks
- `isVatActive(): boolean` - Is the taxpayer active?
- `isVatExempt(): boolean` - Is the taxpayer exempt?
- `isVatUnregistered(): boolean` - Is the taxpayer unregistered?

### EntityPerson

Contains methods:

- `getCompanyName()` - (optional) 
- `getFirstName()` - (optional) 
- `getLastName()` - (optional) 
- `getNip()` - (optional) 
- `getPesel()` - (optional) 

## Exception

### Exception instance

An instance extends PHP Exception and inherits all of the parent methods.

Methods:

- `getMessage(): string` - error message
- `getCode(): string` - example: WL-101
