<?php

use SJRoyd\MF\VATPayers\CheckInFile;

include __DIR__ . "/../vendor/autoload.php";

class Downloader extends CheckInFile
{
    /**
     * Data file location in WEB
     *
     * @var string
     */
    private static $fileServiceLocation = '';

    /**
     * Raw data file location
     *
     * @var string
     */
    private static $fileLocation = '../bin/flat.json';

    /**
     * @return false|int
     */
    public static function downloadNewFile()
    {
        $fhs = fopen(self::$fileServiceLocation, 'r');
        $fhd = fopen(__DIR__ . '/' . self::$fileLocation, 'w');

        return stream_copy_to_stream($fhs, $fhd);
    }

    /**
     * @return false|int
     */
    public static function extractData()
    {
        $data = json_decode(
            file_get_contents(__DIR__ . '/' . self::$fileLocation),
            true
        );

        return file_put_contents(
            __DIR__ . '/' . self::$fileDataLocation,
            serialize($data)
        );
    }
}

//Downloader::downloadNewFile();
Downloader::extractData();